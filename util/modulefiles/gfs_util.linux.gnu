#!/bin/bash

#%Module######################################################################
##

echo "Setting environment variables for gfs_util on Linux with gcc/gfortran"

export GEMINC=/opt/GEMPAK7/gempak/include
export GEMOLB=/opt/GEMPAK7/os/linux64_gfortran/lib
export GEMLIB=/opt/GEMPAK7/os/linux64_gfortran/lib
export OS_INC=/opt/GEMPAK7/os/linux64_gfortran/include

export GPHCNTR_LIB8=/opt/NCEPLIBS-graphics/v2.0.0/linux/libgphcntr_v2.0.0_8.a
export GPHFONT_LIB8=/opt/NCEPLIBS-graphics/v2.0.0/linux/libgphfont_v2.0.0_8.a
export GPH_LIB8=/opt/NCEPLIBS-graphics/v2.0.0/linux/libgph_v2.0.0_8.a
export UTIL_LIB=/opt/NCEPLIBS-graphics/v2.0.0/linux/libutil_v2.0.0.a
export W3G_LIB8=/opt/NCEPLIBS-graphics/v2.0.0/linux/libw3g_v2.0.0_8.a

export NCARG_LIB=/opt/ncl/lib
export NCARG_INC=/opt/ncl/include
