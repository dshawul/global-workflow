#!/bin/bash

#%Module######################################################################
##

echo "Setting environment variables for gdas on Linux with gcc/gfortran"

export NCARG_LIB=/opt/ncl/lib
export NCARG_INC=/opt/ncl/include
