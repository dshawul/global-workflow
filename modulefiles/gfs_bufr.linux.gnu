#!/bin/bash

#%Module######################################################################
##

echo "Setting environment variables for gfs_bufr on Linux with gcc/gfortran"

export myFC=${F90}
export myFCFLAGS="-O3 -g -fopenmp"
export myCPP=/lib/cpp
export myCPPFLAGS="-P"
